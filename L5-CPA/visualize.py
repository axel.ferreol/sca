import numpy as np
import matplotlib.pyplot as plt

# Specify the file path
file_path = '../traces.npy'

# Load data from the .npy file
loaded_data = np.load(file_path)
data_shape = loaded_data.shape

# Assuming the first axis (axis 0) represents the experiments and the second axis (axis 1) represents time
experiment_data = loaded_data[0, :]

# Create time values (assuming a simple example where time is represented by indices)
time_values = np.arange(data_shape[1])

# Plot the data
plt.plot(time_values, experiment_data)

# Add labels and a legend
plt.xlabel('Time')
plt.ylabel('Current')
plt.title('Current(Time) of SubBytes')

# Show the plot
plt.show()