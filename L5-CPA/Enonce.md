* **Le code de l'attque est disponible dans [attack.py](./attack.py)**
* Auteurs : **Antony Perzo - Gaspard Legrand - Axel Ferreol**

# CPA sur AES
Vous disposez d’un AES programmé en langage C dans le dossier [aes_encrypt](../aes_encrypt/). Celui-ci est la cible de ce TP.

Le but est de vous sensibiliser à la cryptanalyse physique. Plus précisément, vous devez réaliser une attaque de type Correlation Power Analysis (CPA) sur le premier tour de l’AES. L’attaque consiste à retrouver la clé de chiffrement de votre AES.

**Toutes les commandes bash sont à exécuter dans un terminal Nix**

## Mesures

**Q1)** Quel est le modèle d’attaquant de cette attaque ? (Quelles capacités, quelle cible, ...)

On considère un attaquant tel que :
* peut mesurer la consommation énergétique de la puce
* connaît l'algorithme utilisé et son implémentation
* peut répéter plusieurs fois l'attaque
* a des connaissances statistique

L'attaquant cherche à découvrir la clé utilisée par AES.

Compilez votre AES à l’aide de `make` dans [aes_encrypt](../aes_encrypt/). Les fichiers générés se trouvent dans le répertoire *bin*.

**Q2)** Ouvrez le fichier [aes.list](../aes_encrypt/bin/aes.list) et comparez le code source de l’AES dans [aes.c](../aes_encrypt/src/aes.c). Que contient [aes.list](../aes_encrypt/bin/aes.list) ?

```aes.list``` contient le code assembleur de ```aes.elf```, qui résulte de la compilation de ```aes.c```.

Vérifiez que votre application fonctionne sur le microcontrôleur. Depuis le répertoire [aes_encrypt](../aes_encrypt/), exécutez le script de test : 
```bash
python3 ../test/J-test-aes.py .
```

Ce script calcule l’AES à la fois sur votre microcontrôleur et sur le PC hôte, puis calcule la différence. L’application fonctionne-t-elle ?

Oui: 

![J-test-aes.py](./Images/screen1.png)

Nous pouvons maintenant lancer les mesures de consommation de courant.


- Lancez les mesures à l’aide du script correspondant
```bash
python3 ../L5-CPA/M-CPA.py [cible] [nombre d’exécutions] [symbole]
```
- La cible est le répertoire de l’application, ici [aes_encrypt](../aes_encrypt/).
- Le nombre d’exécutions est le nombre de fois où nous exécutons un chiffrement AES dont on mesure la consommation.
- Trouvez le nom du symbole (i.e., de la fonction) dont vous voulez la consommation de courant, dans le fichier [aes.c](../aes_encrypt/src/aes.c).

Les fichiers [plaintexts.npy](../aes_encrypt/plaintexts.npy), [ciphertexts.npy](../aes_encrypt/ciphertexts.npy) et [traces.npy](../aes_encrypt/traces.npy) sont alors générés, au format *npy* de numpy.

**Q3)** Expliquez dans le compte rendu ce que vous avez mesuré et pourquoi. Ajoutez une visualisation d’une trace.

On mesure la consommation énergétique de la fonction **SubBytes** parce que:
* la fonction prend en argument une clé AES, donc la valeur de la clé aura une influence sur la consommation énergétique de la fonction.
* l'opération de substitution effectuée par la fonction n'est pas linéaire, on obtiendra donc une corrélation forte uniquement avec la vraie clé.

![Consommation de courant de Subbytes](./Images/screen2.png)

## Prédictions

Pour charger les mesures dans votre script d’analyse, vous avez besoin des librairies *numpy* pour Python ou *NPZ* pour Julia.

**Q4)** Fixez-vous un octet de la clé du premier tour (par exemple, prenez uniquement le premier octet), combien y a-t-il de valeurs possibles pour cet octet de clé ?

Il y a 256 valeurs possibles par octet.


**Q5)** Rappelez le chemin d’attaque (quels sont vos observables) et votre modèle.

Nos observables sont la consommation énergétique de la fonction *SubBytes* pour des *plaintext* connus.

Le chemin d'attaque est le suivant:
* Chiffrer plusieurs *plain texts* et mesurer la consommation de *SubBytes*. On obtient ainsi la consommation de chaque instruction des appels successifs à *SubBytes*
* Choisir un modèle de consommation (dépendant de la clé et du *plain text*). Ici, on choisira le modèle *Hamming Weights*.
* Pour chaque position *i* d'octet dans la clé :
Pour chaque valeur possible d'octet, appliquer le modèle à *SBox(key[i],plaintext[i]* pour tous les *plain texts*.
* Pour chaque octet de la clé, corréler notre modèle avec les mesures. On choisit la valeur de l'octet qui corrèle le mieux sur la tranche [0,50], car il y a 51 instructions dans *SubBytes* et on ne s'intéresse qu'au premier appel à cette fonction (seul le premier round utilise la *master key*).
* On obtient ainsi la clé.


Pour l’octet que vous avez choisi, construisez sa matrice de prédiction **P**.

**Q6)** Quelles sont les dimensions de cette matrice **P** et pourquoi ?

**P** est de dimension : (nb_traces,256).

**Q7)** Les matrices de traces et de prédictions ont-elles les mêmes dimensions ?

Non, la matrice de tracess a pour dimension (nb_traces,510). 510 provient des 51 instructions de la fonction *SubBytes* répétées durant les 10 tours de AES.

## Confrontation

**Q8)** Rappelez ce qu’est un distingueur. Quel distingueur choisissez-vous ici et pourquoi ?

Un distingueur est une fonction permettant de corréler un trace dans un ensemble de traces.

Ici, on doit choisir un distingueur qui mesure la force de la relation linéaire entre notre modèle et la consommation énergétique réelle. On choisit le coeeficient de **Pearson**


Pour l’octet que vous attaquez, confrontez les mesures avec sa matrice de prédiction à l’aide du distingueur choisi.

**Q9)** Tracez les différentes courbes de corrélation pour toutes les hypothèses.

Par exemple, pour le premier octet de la clé : 

![Corrélation pour l'octet n°0 de la clé en fonction des différentes hypothèses de clés](./Images/screen3.png)



**Q10)** Quelle est la valeur de l’octet de clé ciblé ? Est-ce la bonne valeur, sinon quel rang a la bonne valeur ?

Par exemple, pour le premier octet de la clé, on trouve: **0x2b**.


## Attaque complète

Répétez l’attaque pour les autres octets de clé.

**Q11)** Tracez les corrélations correspondantes.


![Corrélations pour les 16 octets de la clé](./Images/screen5.png)

**Q12)** Retrouvez-vous tous les octets ? Sinon, proposez un moyen efficace de les retrouver.

On automatise la lecture du maximum en sélectionnant la valeur de la clé donnant la corrélation maximum pour chaque round.

On obtient: 


![Instructions auxquelles les octets ont une forte corrélation](./Images/screen4.png)



**Q13)** Identifiez à quel instant sont manipulés les différents octets de la clé ? Reliez ces instants à des instructions dans le fichier [aes.list](../aes_encrypt/bin/aes.list).

On lit les instants, correspondant aux instructions : 


![Instructions auxquelles les octets ont une forte corrélation](./Images/screen4.png)



**Q14)** De combien de courbes avez-vous besoin au minimum pour que l’attaque fonctionne ?

Ça marche bien à partir de 250.

**Q15)** Rédigez une conclusion du TP, mentionnez les forces et faiblesses de cette attaque.

Points forts :

* efficace
* rapide
* applicable aux algorithmes chiffrant directement avec la *master key* un text connu

Points faibles :

* connaître précisément la consommation énergétique
* définir un modèle efficace : cela nécessite de connaître la micro-architecture
* nécessite un grand nombre de traces pour éliminer les incertitudes statistiques

