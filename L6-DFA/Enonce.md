* **Le code de l'attque est disponible dans [attack.py](./attack.py)**
* Auteurs : **Antony Perzo - Gaspard Legrand - Axel Ferreol**

# DFA sur AES

## Le code de l'attque est disponible dans [attack.py](./attack.py)
Dans ce TP, vous devez réaliser des injections de fautes pour retrouver la clé de chiffrement. Pour cela, vous devrez réaliser une attaque selon votre préférence.

**Toutes les commandes bash sont à exécuter dans un terminal Nix**

## Identifier l’adresse de l’injection de faute

Le premier objectif est de trouver les paramètres de l'injection de faute : le premier est l’adresse de la faute. Le deuxième est l’itération : au bout de combien d’exécutions de l’instruction à l’adresse d’injection doit-on réellement réaliser la faute. Pour cela, vous disposez du script [K-fault-calibration.py](./K-fault-calibration.py) qui vous permet d’observer le résultat d’injections de faute.

Lancez le script avec

```bash
python3 ../L6-DFA/K-fault-calibration.py [cible] [itération] [symbole]
```
- La cible est le répertoire [aes_encrypt](../aes_encrypt/) contenant l’AES à évaluer.
- L’itération est le nombre au bout duquel, après tant d’exécutions de l’instruction à l’adresse choisie, l’injection a lieu.
- Le symbole est le nom de la fonction que vous voulez explorer.

**Q1)** Qu’affiche le script [K-fault-calibration.py](./K-fault-calibration.py) en sortie ?

Le script affiche pour une fonction donnée et exécutée à un certain moment l'ensemble des instructions auxquelles une faute a été ajouté avant leur exécution. Cela permet de savoir quelles sont les instructions susceptibles de générer un *plain text* différent.

![Exécution de K-fault-calibration.py](./Images/screen1.png)


**Q2)** Quelles itération et symbole faut-il choisir et pourquoi ?

On choisit **ShiftRows** à la **9e** itération.

On choisit ShiftRows car c'est l'opération avant MixColums qui est une opération linéaire.

On choisit la 9e itération, car c'est la dernière, ainsi notre ensemble d'erreurs possibles sera le plus restreint possible.

    python3 ../L6-DFA/K-fault-calibration.py . 9 "ShiftRows"

**Q3)** Quelle adresse d’injection choisissez-vous ?

Pour chaque colonne, il faut trouver une adresse dont la faute modife la colonne.

![Fautes injectées at octets de clé modifiés correspondant](./Images/screen2.png)

*Position* indique l'index des octets modifiés dans la clé.

Ainsi, on peut choisir:
* 0x8000102 pour la première colonne
* 0x8000106 pour la deuxième colonne
* 0x800010a pour la troisième colonne
* 0x800010e pour la quatrième colonne


## Mesures

Le modèle de faute de notre simulation est un saut d’instruction avec une probabilité de succès dépendant de la somme des poids de Hamming des registres.

**Q4)** Quelles attaques théoriques compatibles avec ce modèle de faute connaissez-vous ? **Spécifiez l’attaque choisie pour la suite du TP.**

On peut choisir, soit Non **Uniform Error Value Analysis**, soit **Piret-Quisquater**.

On choisit **Piret-Quisquater** pour la suite.


Pour réaliser une campagne de mesures, lancez le script
```bash
python3 ../L6-DFA/L-DFA.py [cible] [adresse] [nombre d’exécutions] [itération]
```

Les fichiers [ciphertexts.npy](../aes_encrypt/ciphertexts.npy) et [faulty_ciphertexts.npy](../aes_encrypt/faulty_ciphertexts.npy) sont alors générés.

**Q5)** Chargez les fichiers obtenus précédemment et comparez-les. Repérez quels octets ont été fautés. Sont-ils compatibles avec le modèle de faute théorique de l’attaque choisie ?

On exécute :

    python3 ../L6-DFA/L-DFA.py . 0x8000102 100 9

Oui c'est bien cohérent avec notre choix d'adresseet avec le modèle.

![Schéma de l'attaque sur AES](./Images/screen3.png)

## Confrontation (dépend de l’attaque choisie)

**Q6)** Quelle est la cible de votre attaque, précisément ?

On souhaite retrouver le sous-clé n°10.

**Q7)** Décrivez le fonctionnement de votre attaque et son implémentation.

* Pour chaque colonne, on génère de nombreux couples (*ciphertexts*, *faulty ciphertexts*) en ajoutant une erreur qui ne modifie que cette colonne.
* Pour chaque valeur possible de clé (colonne), on calcule *MixColumns(error)*.
* On vérifie si MixColumns(error) appartient à l'ensemble des MixColumns(error) possibles. Sinon, on abandonne cette clé.
* On ne garde que les clés restantes. Il y en a au moins une. Dans le cas où il y en a plusieurs, on refait l'étape précédente sur un plus grand nombre de traces.
* On obtient ainsi la sous clé n°10. Puis, on remonte à la *master key* par bijection entre les sous-clés.

**Q8)** Concluez quelle est la bonne hypothèse de clé.

On trouve les 4 premiers octets de la clé : **2b7e1516**

**Q9)** Si seule une partie de la clé a été obtenue, répétez l’opération pour retrouver la clé complète.

On applique le même procédé pour les 3 autres colonnes, on retrouve la clé complète : **2b7e151628aed2a6abf7158809cf4f3c**

**Q10)** Comparez vos résultats avec la vraie clé.

On a bien la même clé.

**Q11)** Rédigez une conclusion en pointant les forces et faiblesses de cette attaque.

Points forts:
* efficace
* nécessite peu de traces
* l'attaque est parallélisable
* marche sur un large panel d'algorithmes cryptographiques

Points faibles:
* très long