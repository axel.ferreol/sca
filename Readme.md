# Attaques physiques sur AES

Exécuter une application avec des fonctions de sécurité sur un système embarqué demande de faire face à une nouvelle menace: les attaques physiques.
L'attaquant a un accès physique au système faisant tourner votre application, ce qui lui offre de nombreuses possibilités: mesurer des temps d'exécution, écouter l'environnement électromagnétique, ou interagir directement avec le circuit ...

## Mise en place de l'environnement de travail

Avant de coder, il va falloir mettre en place l'environnement de travail:

- une chaine de cross-compilation permettant de compiler le code pour le microcontrôleur depuis votre pc.
- *python* et certaines bibliothèques qui seront utilisés pour émuler la puce et simuler les attaques.
- à vous d'utiliser votre éditeur de code favoris.

### Linux

#### Installation automatique (recommandée)

Installez le gestionnaire de paquets Nix (nécessite les permissions sudo, et *curl*).

```
curl -L https://nixos.org/nix/install | sh
```

Suivre les indications à l'écran pour finaliser l'installation.

Utiliser le fichier *default.nix* comme configuration Nix, par exemple depuis le répertoire du TP

```
cd exercices
nix-shell
```
Vous donne un shell bash avec tout ce qu'il faut pour réaliser les exercices, et sans modifier votre système (en particulier pas besoin de virtualenv pour python).

(default.nix est le fichier par défaut recherché par nix-shell)

#### Installation manuelle (parce que vous ne voulez vraiment pas, mais vraiment pas utiliser Nix)

Commençons par installer la chaine de cross-compilation (commandes données avec apt pour Debian-like).
```
sudo apt install gcc-arm-none-eabi
```

Puis python, son gestionnaire de paquet (pip) et virtualenv.
```
sudo apt install python3
sudo apt install python3-pip
pip3 install virtualenv
```

Nous allons maintenant mettre en place un environnement virtualenv (il faut que l'exécutable soit dans PATH).
```
virtualenv exosenv
```

Enfin, avant chaque session de travail, il faut activer l'environnement:

```
source exosenv/bin/activate
```

**À la fin de la session**, l'environnement est désactivé à l'aide de

```
deactivate
```


Au sein d'une session (après le source, avant le deactivate), installer les bibliothèques python.
```
pip3 install unicorn
pip3 install numpy
pip3 install pyelftools
pip3 install termcolor
```

## Compte rendu et évaluation

Un compte-rendu doit être rendu à la dernière séance. Merci d’envoyer dans une archive .zip contenant :

- vos programmes ;
- un compte rendu au format pdf qui :
    * décrit précisément vos expérimentations,
    * contient les réponses aux questions,
    * est illustré de graphiques (avec légende).

Les questions Qx) des énoncés, dans les dossiers [L5-CPA](./L5-CPA) et [L6-DFA](./L6-DFA/) doivent être explicitement répondue dans le compte rendu, en rappelant le numéro de la question.